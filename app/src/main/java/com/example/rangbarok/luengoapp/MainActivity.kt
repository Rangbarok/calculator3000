package com.example.rangbarok.luengoapp

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.text.Editable
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigDecimal
import java.math.MathContext

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        noiseSwitch.isChecked = true
        auxSwitch.isChecked = true
        auxSwitch.text = "Linear"
        bandwithEditText.setRawInputType(Configuration.KEYBOARD_12KEY)
        data1EditText.setRawInputType(Configuration.KEYBOARD_12KEY)
        data2EditText.setRawInputType(Configuration.KEYBOARD_12KEY)
        resultEditText.isEnabled = false
        checkOptions()

        noiseSwitch.setOnCheckedChangeListener { _,_ ->
            checkOptions()
        }

        auxSwitch.setOnCheckedChangeListener{ _,_ ->
            checkOptions()
        }

        calculateButton.setOnClickListener { _ ->
            calculateResult()
        }
    }

    fun checkOptions() {

        if (noiseSwitch.isChecked) {

            auxSwitch.visibility = View.VISIBLE

            if (auxSwitch.isChecked) {
                // Shannon Lineal (SNR - S and N)
                data1TextView.text = "Signal Power (W) "
                data2TextView.text = "Noise (W) "
                data2TextView.visibility = View.VISIBLE
                data2EditText.visibility = View.VISIBLE
            } else {
                // Shannon (SNR - dB)
                data1TextView.text = "SNR "
                data2TextView.visibility = View.INVISIBLE
                data2EditText.visibility = View.INVISIBLE
            }
        } else {
            // Nyquist
            auxSwitch.visibility = View.INVISIBLE
            data1TextView.text = "Modulation Levels "
            data2TextView.visibility = View.INVISIBLE
            data2EditText.visibility = View.INVISIBLE
        }
    }

    fun calculateResult() {

        if (noiseSwitch.isChecked) {
            if (auxSwitch.isChecked) {
                // Shannon Lineal (SNR - S and N)
                calculateLinearShannon()
            } else {
                // Shannon (SNR - dB)
                calculateShannon()
            }
        } else {
            // Nyquist
            calculateNyquist()
        }
    }

    fun calculateLinearShannon() {

        if (!bandwithEditText.text.isEmpty() && !data1EditText.text.isEmpty() && !data2EditText.text.isEmpty()) {
            var bandwithValue: Double

            if (null != bandwithEditText.text && bandwithEditText.text.toString().contains(',')) {
                var rawBandwithData = bandwithEditText.text.toString().replace(',','.').toDouble()
                bandwithValue = Math.pow(10.0, 6.0) * rawBandwithData
            } else {
                bandwithValue = Math.pow(10.0, 6.0) * bandwithEditText.text.toString().toDouble()
            }

            var signalPower = data1EditText.text.toString().toDouble()
            var noisePower = data2EditText.text.toString().toDouble()
            var logData =  1 + (signalPower / noisePower)
            var maximumCapacity = BigDecimal(bandwithValue, MathContext.DECIMAL64) * log2(logData)
            resultEditText.text = Editable.Factory.getInstance().newEditable(maximumCapacity.toDouble().toString())
        }
    }

    fun calculateShannon() {

        if (!bandwithEditText.text.isEmpty() && !data1EditText.text.isEmpty()) {
            var bandwithValue: Double

            if (null != bandwithEditText.text && bandwithEditText.text.toString().contains(',')) {
                var rawBandwithData = bandwithEditText.text.toString().replace(',','.').toDouble()
                bandwithValue = Math.pow(10.0, 6.0) * rawBandwithData
            } else {
                bandwithValue = Math.pow(10.0, 6.0) * bandwithEditText.text.toString().toDouble()
            }

            var logData = 1 + data1EditText.text.toString().toDouble()
            var log2Result = log2(logData)
            var maximumCapacity = BigDecimal(bandwithValue, MathContext.DECIMAL64) * log2Result
            resultEditText.text = Editable.Factory.getInstance().newEditable(maximumCapacity.toDouble().toString())
        }
    }

    fun calculateNyquist() {

        if (!bandwithEditText.text.isEmpty() && !data1EditText.text.isEmpty()) {
            var bandwithValue: Double

            if (null != bandwithEditText.text && bandwithEditText.text.toString().contains(',')) {
                var rawBandwithData = bandwithEditText.text.toString().replace(',','.').toDouble()
                bandwithValue = Math.pow(10.0, 6.0) * rawBandwithData
            } else {
                bandwithValue = Math.pow(10.0, 6.0) * bandwithEditText.text.toString().toDouble()
            }

            var modulationLevels = data1EditText.text.toString().toDouble()
            var maximumCapacity = BigDecimal(2.0, MathContext.DECIMAL128) * BigDecimal(bandwithValue, MathContext.DECIMAL64) * log2(modulationLevels)
            resultEditText.text =  Editable.Factory.getInstance().newEditable(maximumCapacity.toDouble().toString())
        }
    }

    fun log2 (x: Double): BigDecimal {

        return BigDecimal((Math.log(x) / Math.log(2.0)), MathContext.DECIMAL128)
    }
}